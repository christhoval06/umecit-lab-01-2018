<?php
/**
 * Created by PhpStorm.
 * User: christhoval
 * Date: 11/13/18
 * Time: 8:54 AM
 */


function datosform($variables, $req)
{
    $datos = array();
    $variables = explode(',', $variables);
    foreach ($variables as $var) {
        foreach ($req as $campo => $valor) {
            if ($var == $campo)
                $datos[] = $valor;
        }
    }
    return $datos;
}
