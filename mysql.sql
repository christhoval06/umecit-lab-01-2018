DROP TABLE IF EXISTS `lab01app`.`users`;
CREATE TABLE `lab01app`.`users` (
`id` INT(11) NOT NULL AUTO_INCREMENT ,
`name` VARCHAR(50) NOT NULL ,
`username` VARCHAR(25) NOT NULL ,
`password` VARCHAR(100) NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE = InnoDB;