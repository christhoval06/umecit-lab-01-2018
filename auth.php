<?php
/**
 * Created by PhpStorm.
 * User: christhoval
 * Date: 11/13/18
 * Time: 8:54 AM
 */
header('Content-Type: application/json');
include_once "mysql.class.php";
include_once "utils.php";


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $mysql = mysql::getInstance();
    list($username, $password) = datosform('username,password', $_POST);


    list($status, $query) = $mysql->runSQL("select * from users where username='$username';", 2);

    $res = array(
        "success" => $status
    );

    if (!empty($query)) {
        list($id, $name, $username_db, $password_db) = $query[0];
        $auth = $password == $password_db;
        $res = array_merge($res, array(
            "success" => $auth,
            "msg" => $auth ? 'W3lcome' : 'invalid credentials',
            "data" => $auth ? array(
                "name" => $name,
                "username" => $username,
            ) : null
        ));
    } else {
        $res = array_merge($res, array(
            "success" => empty($res),
            "msg" => 'user not found!!',
            "data" => array(
                "username" => $username,
            )
        ));
    }
    echo json_encode($res);
} else {
    echo json_encode(array(
        "success" => false,
        "msg" => 'Only valid POST',
        "data" => array()
    ));
}