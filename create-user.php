<?php
/**
 * Created by PhpStorm.
 * User: christhoval
 * Date: 11/13/18
 * Time: 8:54 AM
 */
header('Content-Type: application/json');
include_once "mysql.class.php";
include_once "utils.php";


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $mysql = mysql::getInstance();
    list($name, $username, $password) = datosform('name,username,password', $_POST);

    $sql = "insert into users (name,username,password) values ('$name','$username','$password');";
    // $sql="Update grupos set nombre='$nombre',activo=$activo,nota='$nota',uc='$uc' where grupoid=$grupoid;";

    list($success, $msg) = $mysql->exeSQL($sql);

    $res = array(
        "success" => $success
    );
    if ($success) {
        $res = array_merge($res, array(
            "msg" => 'user created!!',
            "data" => array(
                "name" => $name,
                "username" => $username,
                "password" => $password
            )
        ));
    } else {
        $res = array_merge($res, array(
            "msg" => "Can't create user, please try again",
            "data" => null
        ));
    }

    echo json_encode($res);
} else {
    echo json_encode(array(
        "success" => false,
        "msg" => 'Only valid POST',
        "data" => array()
    ));
}